import {Domain, Member} from "../../src/modules/members/core/entities/member";
import {Mission, Status} from "../../src/modules/members/core/entities/mission";
import {OnboardMemberDto} from "../../src/modules/members/usecases/onboardMember";

export const memberFactory = (dto: OnboardMemberDto): Member => {
    const first_mission = Mission.create({
        start: new Date(dto.missionStart),
        end: new Date(dto.missionEnd),
        status: dto.missionStatus as Status,
        employer: dto.missionEmployer,
    });

    return Member.create({
        username: `${dto.firstname}.${dto.lastname}`,
        fullname: `${dto.firstname} ${dto.lastname}`,
        missions: [first_mission],
        domain: dto.domain as Domain,
        githubUsername: dto.githubUsername,
        personalEmail: dto.personalEmail,
    });
};
