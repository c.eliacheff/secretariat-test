import { FakeChatService } from '../../src/shared/notifications/FakeChatService';
import {
    MessageDto,
    notifyChatOnChannel,
} from '../../src/shared/notifications/lib';

describe('Chat notifications', () => {
    it('should send chat notification on given channel', async () => {
        const chatService = new FakeChatService();

        const res = await chatService.send('general', 'toto');
        expect(res).toBeUndefined();
    });

    it('should throw when missing message', async () => {
        const chatService = new FakeChatService();
        const sendMessageDto = {
            channel: 'general',
        };

        const res = notifyChatOnChannel(chatService)(sendMessageDto);
        await expect(res).rejects.toThrow('Error sending notification');
    });

    it('should send chat notification on given channel', async () => {
        const chatService = new FakeChatService();
        const sendMessageDto: MessageDto = {
            channel: 'general',
            message: 'random message',
        };

        const res = await notifyChatOnChannel(chatService)(sendMessageDto);
        expect(res.events).toBeTruthy();
    });
});
