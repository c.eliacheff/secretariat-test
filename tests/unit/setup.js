global.console = {
    // Keep native behaviour for other methods
    log: console.log,
    error: console.error,
    warn: console.warn,
    info: console.info,
    debug: jest.fn(), // console.debug are ignored in tests
};
