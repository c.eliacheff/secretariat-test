import { MemberCreated } from '../../src/modules/members/core/events';
import { InMemoryMembersRepository } from '../../src/modules/members/infra/inMemoryMembersRepository';
import { getMemberByUsername } from '../../src/modules/members/usecases/getMember';
import { listMembers } from '../../src/modules/members/usecases/listMembers';
import {
    onboardMember,
    OnboardMemberDto,
} from '../../src/modules/members/usecases/onboardMember';
import { ConstantDateProvider } from '../../src/shared/utils/constantDateProvider';
import { DateProvider } from '../../src/shared/utils/dateProvider';
import { memberFactory } from './member.factory';

describe('User onboarding', () => {
    it('should onboard a new member with data', async () => {
        const membersRepository = new InMemoryMembersRepository();
        const dateProvider: DateProvider = new ConstantDateProvider(new Date());

        const res = await onboardMember(
            membersRepository,
            dateProvider
        )(sampleActiveMemberDto);

        expect(membersRepository.members.size).toEqual(1);
        expect([...membersRepository.members.values()][0]).toEqual(JulienUser);
        expect(res.events[0]).toBeInstanceOf(MemberCreated);
        expect((<MemberCreated>res.events[0]).member).toEqual(JulienUser);
        expect((<MemberCreated>res.events[0]).occurredAt).toEqual(
            dateProvider.now()
        );
    });
});

describe('Listing users', () => {
    let membersRepository: InMemoryMembersRepository;

    beforeEach(() => {
        membersRepository = new InMemoryMembersRepository();
    });

    it('should return empty array if no members', async () => {
        const { result } = await listMembers(membersRepository)();

        expect(membersRepository.members.size).toEqual(0);
        expect(result).toEqual([]);
    });

    it('should return a list of existing members', async () => {
        membersRepository.feedWith([JulienUser]);

        const { result } = await listMembers(membersRepository)();

        expect(result).toEqual([JulienUser]);
    });

    it('should get a member by username', async () => {
        membersRepository.feedWith([JulienUser]);

        const { result } = await getMemberByUsername(membersRepository)(
            JulienUser.username
        );

        expect(result).toEqual(JulienUser);
    });
});

describe('Updating an user mission', () => {
    let membersRepository: InMemoryMembersRepository;

    beforeEach(() => {
        membersRepository = new InMemoryMembersRepository();
        membersRepository.feedWith([JulienUser]);
    });

    it("should update member's current mission end date", async () => null);
});

const sampleActiveMemberDto: OnboardMemberDto = {
    firstname: 'julien',
    lastname: 'dauphant',
    missionStart: '2020-02-21',
    missionEnd: '2023-02-21',
    missionStatus: 'admin',
    missionEmployer: 'DINUM',
    domain: 'Coaching',
    githubUsername: '@juliendauphant',
    personalEmail: 'juliend@gmail.com',
    referent: 'unknown',
};

const JulienUser = memberFactory(sampleActiveMemberDto);
