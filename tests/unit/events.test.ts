import { DomainEvent } from '../../src/shared';
import { EventBus } from '../../src/shared/eventBus/eventBus';
import { ConstantDateProvider } from '../../src/shared/utils/constantDateProvider';

describe('Event bus', () => {
    let now: number;
    let fakeEvent: DomainEvent;

    beforeEach(() => {
        now = new ConstantDateProvider(new Date()).now();
        fakeEvent = new FakeEventOccurred('test');
    });

    it('should subscribe to an event', () => {
        const bus = new EventBus();

        bus.subscribe(FakeEventOccurred.name, () => void 0);

        expect(
            {}.hasOwnProperty.call(bus.handlersMap, FakeEventOccurred.name)
        ).toBeTruthy();
        expect(bus.handlersMap[FakeEventOccurred.name]).toHaveLength(1);
    });

    it('should dispatch one event', (done) => {
        const bus = new EventBus();

        bus.subscribe(FakeEventOccurred.name, (event: FakeEventOccurred) => {
            expect(event.data).toBe('test');
            expect(event.occurredAt).toEqual(now);
            done();
        });

        bus.dispatch(fakeEvent);
    });

    it('should handle multiple events and subscriptions', (done) => {
        const bus = new EventBus();

        bus.subscribe(FakeEventOccurred.name, (event: FakeEventOccurred) => {
            expect(event.occurredAt).toEqual(now);
        });

        bus.subscribe(FakeEventOccurred.name, (event: FakeEventOccurred) => {
            expect(event.occurredAt).toEqual(now);
            done();
        });

        expect(bus.handlersMap[FakeEventOccurred.name]).toHaveLength(2);
        bus.dispatchAll([fakeEvent]);
    });
});

class FakeEventOccurred implements DomainEvent {
    public occurredAt: number;

    constructor(public data: string, now?: number) {
        this.occurredAt = now || Date.now();
    }
}
