import { UserLoggedIn } from '../../src/modules/auth/core/events';
import { User } from '../../src/modules/auth/core/user';
import { InMemoryUserService } from '../../src/modules/auth/infra/inMemoryUserService';
import { login } from '../../src/modules/auth/usecases/login';
import { FakeTokenService } from '../../src/shared/utils/fakeTokenService';
import { TokenService } from '../../src/shared/utils/tokenService';

const demoUsers = [new User('julien.dauphant'), new User('lucas.charrier')];

describe('Autorization', () => {
    let userService: InMemoryUserService;
    let tokenService: TokenService;

    beforeEach(() => {
        userService = new InMemoryUserService().feedWith(demoUsers);
        tokenService = new FakeTokenService().setValue('toto');
    });
    it('should return a token when a valid user login', async () => {
        const result = await login(
            userService,
            tokenService
        )({ username: 'julien.dauphant' });

        expect(result.result).toEqual('toto');
        expect(result.events[0]).toBeInstanceOf(UserLoggedIn);
        expect((<UserLoggedIn>result.events[0]).user.username).toEqual(
            'julien.dauphant'
        );
    });

    it('should throw when an non-existing user login', async () => {
        const result = login(
            userService,
            tokenService
        )({ username: 'donald.duck' });

        await expect(result).rejects.toThrow('Invalid user');
    });
});
