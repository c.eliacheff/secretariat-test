import { ConsoleEmailService } from '../../src/shared/email/ConsoleEmailService';

describe('Email notifications', () => {
    it('should send email notification with from/to/subject/body', async () => {
        const emailService = new ConsoleEmailService();

        const res = await emailService.send('test@test.com', 'test');
        expect(res).toBeUndefined();
    });
});
