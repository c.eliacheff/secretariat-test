import { Knex } from 'knex';

export const config: Knex.Config = {
    client: 'pg',
    connection: {
        socketPath: '/path/to/socket.sock',
        user: 'your_database_user',
        password: 'your_database_password',
        database: 'myapp_test',
    },
    migrations: {
        directory: 'src/db/migrations',
        tableName: 'knex_migrations',
        extension: 'ts',
    },
    seeds: {
        directory: 'src/db/test-seeds',
    },
};

let db = null;

if (process.env.NODE_ENV === 'prod') {
    console.log('process.env.NODE_ENV', process.env.NODE_ENV);
    db = new (Knex as any)(config);
}

export default db;
