import { TokenService } from './tokenService';

export class FakeTokenService implements TokenService {
    private token: string;

    setValue(token: string) {
        this.token = token;
        return this;
    }

    createToken(): Promise<string> {
        return Promise.resolve(this.token);
    }
}
