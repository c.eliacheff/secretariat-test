import { DateProvider } from './dateProvider';

export class SystemDateProvider implements DateProvider {
    now(): number {
        return Date.now();
    }
}
