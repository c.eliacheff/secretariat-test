import { DateProvider } from './dateProvider';

export class ConstantDateProvider implements DateProvider {
    constructor(private _date: Date) {}

    now(): number {
        return this._date.getTime();
    }
}
