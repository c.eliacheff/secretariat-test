import EmailService from './emailService';
import { ConsoleEmailService } from './ConsoleEmailService';
import { MailjetEmailService } from './MailjetEmailService';

const emailService: EmailService =
    process.env.environment === 'prod'
        ? new MailjetEmailService()
        : new ConsoleEmailService();

export { emailService };
