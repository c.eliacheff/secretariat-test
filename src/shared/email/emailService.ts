export default interface EmailService {
    send(to: string | string[], body: string): Promise<void>;
}
