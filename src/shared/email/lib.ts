import { Usecase } from '../types';
import EmailService from './emailService';

interface EmailDto {
    to: string | string[];
    body: string;
}

export const sendEmail: Usecase<void> =
    (emailService: EmailService) => async (messageDto: EmailDto) => {
        if (!messageDto.to || !messageDto.body) {
            throw new Error('Error sending email');
        }

        try {
            await emailService.send(messageDto.to, messageDto.body);
        } catch (err) {
            throw new Error('Error sending email:' + err);
        }

        return {
            events: [],
        };
    };
