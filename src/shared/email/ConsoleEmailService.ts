import EmailService from './emailService';

export class ConsoleEmailService implements EmailService {
    send(to: string | string[], body: string): Promise<void> {
        console.info(`[Email] Sent`);
        const recipients = Array.isArray(to) ? to.join(', ') : to;
        console.info('To: ', recipients, '\nBody: ', body, '\n');

        return Promise.resolve();
    }
}
