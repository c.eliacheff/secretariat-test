export interface DomainEvent {
    occurredAt: number;
}

export type Usecase<T> = (
    ...deps: unknown[]
) => (
    ...args: unknown[]
) => Promise<{ result?: T extends void ? never : T; events?: DomainEvent[] }>;
