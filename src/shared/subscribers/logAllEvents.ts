import { EventBus } from '../eventBus/eventBus';
import { DomainEvent } from '../types';

export class LogAllEvents {
    constructor(private bus: EventBus) {
        this.setupSubscriptions();
    }

    setupSubscriptions(): void {
        this.bus.subscribe('*', this.onEvent.bind(this));
    }

    private async onEvent(event: DomainEvent) {
        // Log Event
        console.log(`[DomainEvent] (${event.constructor.name}) => ${event}`);
    }
}
