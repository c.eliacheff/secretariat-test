import { EventBus } from './eventBus/eventBus';
import { LogAllEvents } from './subscribers/logAllEvents';
import { DateProvider } from './utils/dateProvider';
import db from './utils/db';
import { FakeTokenService } from './utils/fakeTokenService';
import { SystemDateProvider } from './utils/systemDateProvider';

const bus = new EventBus();
const dateProvider: DateProvider = new SystemDateProvider();

const tokenService =
    process.env.environment === 'prod'
        ? new FakeTokenService()
        : new FakeTokenService();

new LogAllEvents(bus);

export { db, dateProvider, bus as eventBus, tokenService };
