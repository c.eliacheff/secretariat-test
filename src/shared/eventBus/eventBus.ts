import { DomainEvent } from '../types';

interface EventMap {
    [name: string]: ((event: DomainEvent) => void)[];
}

// Todo : static
export class EventBus {
    handlersMap: EventMap = {};

    subscribe(name: string, callback: (data: unknown) => void): void {
        if (!{}.hasOwnProperty.call(this.handlersMap, name)) {
            this.handlersMap[name] = [];
        }
        this.handlersMap[name].push(callback);
    }

    dispatch(event: DomainEvent): void {
        const eventClass = event.constructor.name;

        if ({}.hasOwnProperty.call(this.handlersMap, eventClass)) {
            const handlers = this.getLocalAndGlobalHandlers(eventClass);
            handlers.map((handler) => handler(event));
        }
    }

    private getLocalAndGlobalHandlers(
        eventClass: string
    ): ((event: DomainEvent) => void)[] {
        const handlers = this.handlersMap[eventClass];
        if ({}.hasOwnProperty.call(this.handlersMap, '*')) {
            handlers.concat(this.handlersMap['*']);
        }
        return handlers;
    }

    dispatchAll(events: DomainEvent[]): void {
        events.map((event) => this.dispatch(event));
    }
}
