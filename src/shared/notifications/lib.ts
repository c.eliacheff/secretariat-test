import { Usecase } from '../types';
import ChatService, { ChatChannels } from './chatService';

export interface MessageDto {
    channel: ChatChannels;
    message: string;
}

export const notifyChatOnChannel: Usecase<void> =
    (chatService: ChatService) => async (messageDto: MessageDto) => {
        if (!messageDto.channel || !messageDto.message) {
            throw new Error('Error sending notification');
        }

        try {
            await chatService.send(messageDto.channel, messageDto.message);
        } catch (err) {
            throw new Error('Error sending notification:' + err);
        }

        return {
            events: [],
        };
    };
