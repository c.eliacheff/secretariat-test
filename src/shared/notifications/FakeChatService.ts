import ChatService, { ChatChannels } from './chatService';

export class FakeChatService implements ChatService {
    send(channel: ChatChannels, text: string): Promise<void> {
        console.info(`[Notification] (${channel}) ${text}`);
        return Promise.resolve();
    }
}
