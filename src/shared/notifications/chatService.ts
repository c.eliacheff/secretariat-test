export type ChatChannels = 'general' | 'secretariat' | 'dev';

export default interface ChatService {
    send(channel: ChatChannels, text: string): Promise<void>;
}
