import ChatService from './chatService';
import { FakeChatService } from './FakeChatService';
import { MattermostChatService } from './MattermostChatService';

const chatService: ChatService =
    process.env.environment === 'prod'
        ? new MattermostChatService()
        : new FakeChatService();

export { chatService };
