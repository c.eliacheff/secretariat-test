import { memberFactory } from '../../../tests/unit/member.factory';
import { eventBus } from '../../shared';
import { emailService } from '../../shared/email/config';
import { chatService } from '../../shared/notifications/config';
import { AfterUserLoggedIn } from '../auth/subscribers/afterUserLoggedIn';
import { InMemoryMembersRepository } from './infra/inMemoryMembersRepository';
import { RealMembersRepository } from './infra/realMembersRepository';
import { AfterMemberCreated } from './subscribers/afterMemberCreated';

const fakeData = [
    memberFactory({
        firstname: 'Julien',
        lastname: 'Dauphant',
        missionStart: '2020-02-21',
        missionEnd: '2023-02-21',
        missionStatus: 'admin',
        missionEmployer: 'DINUM',
        domain: 'Coaching',
        githubUsername: '@juliendauphant',
        personalEmail: 'jd@gmail.com',
        referent: 'unknown',
    }),
    memberFactory({
        firstname: 'Lucas',
        lastname: 'Charrier',
        missionStart: '2020-02-21',
        missionEnd: '2023-02-21',
        missionStatus: 'admin',
        missionEmployer: 'DINUM',
        domain: 'Coaching',
        githubUsername: '@lucascharrier',
        personalEmail: 'lc@gmail.com',
        referent: 'unknown',
    }),
];

const membersRepository =
    process.env.environment === 'prod'
        ? new RealMembersRepository()
        : new InMemoryMembersRepository().feedWith(fakeData);

new AfterMemberCreated(chatService, eventBus);
new AfterUserLoggedIn(emailService, eventBus);

export { membersRepository };
