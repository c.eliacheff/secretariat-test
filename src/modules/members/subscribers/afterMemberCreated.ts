import { EventBus } from '../../../shared';
import ChatService from '../../../shared/notifications/chatService';
import { MemberCreated } from '../core/events';

export class AfterMemberCreated {
    constructor(
        private notificationService: ChatService,
        private bus: EventBus
    ) {
        this.setupSubscriptions();
    }

    setupSubscriptions(): void {
        this.bus.subscribe(MemberCreated.name, this.onMemberCreated.bind(this));
    }

    private async onMemberCreated(event: MemberCreated) {
        await this.notificationService.send(
            'general',
            'Welcome ' + event.member.fullname
        );
    }
}
