import { DomainEvent } from '../../../shared';
import { Member } from './entities/member';

export class MemberCreated implements DomainEvent {
    occurredAt: number;

    constructor(public member: Member, now?: number) {
        this.occurredAt = now || Date.now();
    }
}
