import { Member } from '../entities/member';

export interface MembersRepository {
    add(member: Member): Promise<void>;

    all(): Promise<Member[]>;

    getByUsername(username: string): Promise<Member | null>;
}
