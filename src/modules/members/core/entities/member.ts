import { Mission } from './mission';

export type Domain =
    | 'Animation'
    | 'Coaching'
    | 'Déploiement'
    | 'Design'
    | 'Développement'
    | 'Intraprenariat'
    | 'Produit'
    | 'Autre';

export type MemberProps = {
    username: string;
    fullname: string;
    githubUsername: string;
    personalEmail: string;
    missions: Mission[];
    domain: Domain;
    referent?: Member;
    mattermostUsername?: string;
    bio?: string;
    website?: string;
};

export class Member {
    private constructor(private props: MemberProps) {}

    get bio() {
        return this.props.bio;
    }

    get domain() {
        return this.props.domain;
    }

    get fullname() {
        return this.props.fullname;
    }

    get githubUsername() {
        return this.props.githubUsername;
    }

    get mattermostUsername() {
        return this.props.mattermostUsername;
    }

    get missions() {
        return this.props.missions;
    }

    get personalEmail() {
        return this.props.personalEmail;
    }

    get referent() {
        return this.props.referent;
    }

    get website() {
        return this.props.website;
    }

    get username() {
        return this.props.username;
    }

    public static create(data: MemberProps) {
        // TODO check data consistency + validation (VO + null check/guard)
        return new Member(data);
    }
}
