export type Status = 'independant' | 'admin' | 'service';

export type MissionProps = {
    start: Date;
    end: Date;
    status: Status;
    employer: string;
};

export class Mission {
    private constructor(private props: MissionProps) {}

    static create(data: MissionProps): Mission {
        return new Mission({
            start: data.start,
            end: data.end,
            status: data.status,
            employer: data.employer,
        });
    }
}
