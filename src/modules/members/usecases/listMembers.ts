import { Usecase } from '../../../shared';
import { Member } from '../core/entities/member';
import { MembersRepository } from '../core/gateways/membersRepository';

export const listMembers: Usecase<Member[]> =
    (memberRepository: MembersRepository) => async () => {
        const members = await memberRepository.all();
        return {
            result: members,
        };
    };
