import { Usecase } from '../../../shared';
import { Member } from '../core/entities/member';
import { MembersRepository } from '../core/gateways/membersRepository';

export const getMemberByUsername: Usecase<Member> =
    (membersRepository: MembersRepository) => async (username: string) => {
        const member = await membersRepository.getByUsername(username);
        return {
            result: member,
        };
    };
