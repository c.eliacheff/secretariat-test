import { Usecase } from '../../../shared';
import { DateProvider } from '../../../shared/utils/dateProvider';
import { MemberCreated } from '../core/events';
import { Domain, Member } from '../core/entities/member';
import { Mission, Status } from '../core/entities/mission';
import { MembersRepository } from '../core/gateways/membersRepository';

export interface OnboardMemberDto {
    firstname: string;
    lastname: string;
    githubUsername: string;
    personalEmail: string;
    missionStart: string;
    missionEnd: string;
    missionStatus: string;
    missionEmployer: string;
    domain: string;
    referent: string;
    bio?: string;
    website?: string;
}

export const onboardMember: Usecase<void> =
    (memberRepository: MembersRepository, dateProvider: DateProvider) =>
    async (onboardMemberDto: OnboardMemberDto) => {
        const first_mission = Mission.create({
            start: new Date(onboardMemberDto.missionStart),
            end: new Date(onboardMemberDto.missionEnd),
            status: onboardMemberDto.missionStatus as Status,
            employer: onboardMemberDto.missionEmployer,
        });

        const member = Member.create({
            username: `${onboardMemberDto.firstname}.${onboardMemberDto.lastname}`,
            fullname: `${onboardMemberDto.firstname} ${onboardMemberDto.lastname}`,
            missions: [first_mission],
            domain: onboardMemberDto.domain as Domain,
            githubUsername: onboardMemberDto.githubUsername,
            personalEmail: onboardMemberDto.personalEmail,
        });

        await memberRepository.add(member);
        return { events: [new MemberCreated(member, dateProvider.now())] };
    };
