import { Router } from 'express';
import { dateProvider, eventBus } from '../../shared/config';
import { membersRepository } from './dependencies';
import { getMemberByUsername } from './usecases/getMember';
import { listMembers } from './usecases/listMembers';
import { onboardMember } from './usecases/onboardMember';

const membersRouter = Router();

membersRouter.get('/', async (req, res) => {
    const { result } = await listMembers(membersRepository)();
    res.render('members', { members: result });
});

membersRouter.get('/onboarding', async (req, res) => {
    res.render('member-onboarding.ejs');
});

membersRouter.get('/me', async (req, res) => {
    const { result } = await getMemberByUsername(membersRepository)(
        req.user.username
    );
    res.render('member-current.ejs', { member: result });
});

membersRouter.post('/', async (req, res) => {
    const { events } = await onboardMember(
        membersRepository,
        dateProvider
    )(req.body);
    eventBus.dispatchAll(events);
    res.send();
});

export { membersRouter };
