import { db } from '../../../shared';
import { Member } from '../core/entities/member';
import { MembersRepository } from '../core/gateways/membersRepository';

export class RealMembersRepository implements MembersRepository {
    async add(member: Member): Promise<void> {
        // mapper - to db
        await db('members').insert(member);
        return;
    }

    all(): Promise<Member[]> {
        const res = db('members').select();
        // mapper - to domain
        return res;
    }

    getByUsername(username: string): Promise<Member | null> {
        const res = db('members')
            .select('*')
            .where('username', username)
            .first();
        // mapper - to domain
        return res;
    }
}
