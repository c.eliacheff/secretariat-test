import { Member } from '../core/entities/member';
import { MembersRepository } from '../core/gateways/membersRepository';

export class InMemoryMembersRepository implements MembersRepository {
    public members: Map<string, Member> = new Map<string, Member>();

    async add(member: Member): Promise<void> {
        this.members.set(member.username, member);
        return;
    }

    feedWith(members: Member[]): InMemoryMembersRepository {
        for (const member of members) {
            this.add(member).then();
        }
        return this;
    }

    all(): Promise<Member[]> {
        return Promise.resolve([...this.members.values()]);
    }

    getByUsername(username: string): Promise<Member | null> {
        return Promise.resolve(
            [...this.members.values()].find((m) => m.username === username)
        );
    }
}
