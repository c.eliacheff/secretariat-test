import { Member } from '../core/entities/member';

export class MemberMapper {
    toDomain(raw: any) {
        return Member.create({
            bio: raw.bio,
            domain: raw.domain,
            fullname: raw.fullname,
            githubUsername: raw.githubUsername,
            mattermostUsername: raw.mattermostUsername,
            missions: raw.missions,
            personalEmail: raw.personalEmail,
            referent: raw.referent,
            username: raw.username,
            website: raw.website,
        });
    }

    toDatabase(member: Member) {
        return {
            bio: member.bio,
            domain: member.domain,
            fullname: member.fullname,
            githubUsername: member.githubUsername,
            mattermostUsername: member.mattermostUsername,
            missions: member.missions,
            personalEmail: member.personalEmail,
            referent: member.referent,
            username: member.username,
            website: member.website,
        };
    }
}
