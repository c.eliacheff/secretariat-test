import { EventBus } from '../../../shared';
import EmailService from '../../../shared/email/emailService';
import { UserLoggedIn } from '../core/events';

export class AfterUserLoggedIn {
    constructor(private emailService: EmailService, private bus: EventBus) {
        this.setupSubscriptions();
    }

    setupSubscriptions(): void {
        this.bus.subscribe(UserLoggedIn.name, this.onUserLoggedIn.bind(this));
    }

    private async onUserLoggedIn(event: UserLoggedIn) {
        await this.emailService.send(
            'Login: ' + event.user.username,
            'Token: ' + event.token
        );
    }
}
