import { User } from './user';

export interface UserService {
    get(username: string): Promise<User>;
}
