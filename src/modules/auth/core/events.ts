import { DomainEvent } from '../../../shared';
import { User } from './user';

export class UserLoggedIn implements DomainEvent {
    occurredAt: number;

    constructor(public user: User, public token: string, now?: number) {
        this.occurredAt = now || Date.now();
    }
}
