import { Usecase } from '../../../shared';
import { TokenService } from '../../../shared/utils/tokenService';
import { UserLoggedIn } from '../core/events';
import { UserService } from '../core/userService';

interface LoginDto {
    username: string;
}

export const login: Usecase<string> =
    (userService: UserService, tokenService: TokenService) =>
    async (loginDto: LoginDto) => {
        const user = await userService.get(loginDto.username);
        if (!user) {
            throw new Error('Invalid user');
        }
        const token = await tokenService.createToken();
        return {
            result: token,
            events: [new UserLoggedIn(user, token)],
        };
    };
