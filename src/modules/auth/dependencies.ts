import { User } from './core/user';
import { InMemoryUserService } from './infra/inMemoryUserService';

const demoUsers = [new User('julien.dauphant'), new User('lucas.charrier')];

const userService =
    process.env.environment === 'prod'
        ? new InMemoryUserService()
        : new InMemoryUserService().feedWith(demoUsers);

export { userService };
