import { Router } from 'express';
import { eventBus, tokenService } from '../../shared';
import { userService } from './dependencies';
import { login } from './usecases/login';

const authRouter = Router();

authRouter.get('/login', async (req, res) => {
    console.log(req.user);
    res.render('login');
});

authRouter.post('/login', async (req, res) => {
    const { events } = await login(userService, tokenService)(req.body);
    eventBus.dispatchAll(events);
    res.redirect('/auth/login');
});

export { authRouter };
