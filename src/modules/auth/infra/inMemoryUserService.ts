import { User } from '../core/user';
import { UserService } from '../core/userService';

export class InMemoryUserService implements UserService {
    public users: Map<string, User> = new Map<string, User>();

    feedWith(users: User[]): InMemoryUserService {
        for (const user of users) {
            this.users.set(user.username, user);
        }
        return this;
    }

    get(username: string): Promise<User> {
        return Promise.resolve(
            this.users.get(username)
            // [...this.users.values()].find((u) => u.username === username)
        );
    }
}
