import bodyParser from 'body-parser';
import cookieParser from 'cookie-parser';
import cors from 'cors';
import express from 'express';
import expressJWT from 'express-jwt';
import jwt from 'jsonwebtoken';
import helmet from 'helmet';
import path from 'path';
import router from './router';

const app = express();

app.use(helmet());
app.use(cors());

app.set('view engine', 'ejs');
app.set('views', path.join(__dirname, '/views'));

app.get('/health', (req, res) => {
    res.send('App is up');
});

app.use(cookieParser(process.env.SECRET || 'test'));
app.use(bodyParser.urlencoded({ extended: false }));

// Auth Middlewares
app.use(
    // Set result in req.user
    expressJWT({
        secret: process.env.SECRET || 'test',
        algorithms: ['HS256'],
        getToken: (req) => req.cookies.token || null,
    }).unless({
        path: [
            '/',
            '/auth/login',
            '/api/auth/login',
            '/mentorship/accept',
            '/mentorship/decline',
            '/notifications/github',
            '/members/onboarding',
            '/members/onboarding/success',
        ],
    })
);

// Save a token in cookie that expire after 7 days if user is logged
app.use((req, res, next) => {
    if (req.user && req.user.username) {
        const getJwtTokenForUser = (username) =>
            jwt.sign({ username }, 'toto', { expiresIn: '7 days' });
        res.cookie('token', getJwtTokenForUser(req.user.username), {
            sameSite: 'lax',
        });
    }
    next();
});

app.use((err, req, res, next) => {
    if (err.name === 'UnauthorizedError') {
        // redirect to login and keep the requested url in the '?next=' query param
        if (req.method === 'GET') {
            req.flash(
                'error',
                "Vous n'êtes pas identifié pour accéder à cette page (ou votre accès n'est plus valide)"
            );
            const nextParam = req.url ? `?next=${req.url}` : '';
            return res.redirect(`/login${nextParam}`);
        }
    }
    return next(err);
});
// End auth middlewares

app.use('/', router);

export default app;
