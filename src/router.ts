import { Router } from 'express';
import { authRouter } from './modules/auth/auth.controllers';
import { membersRouter } from './modules/members/members.controllers';

const router: Router = Router();
router.use('/auth', authRouter);
router.use('/members', membersRouter);

export default router;
