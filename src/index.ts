import * as dotenv from 'dotenv';
import app from './server';

dotenv.config();

const PORT: number = parseInt(process.env.PORT as string, 10) || 3000;

app.listen(PORT, () => {
    console.log(`Listening on port ${PORT}`);
});
